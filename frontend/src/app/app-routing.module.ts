import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@app/modules/home/home.component';
import { ResultsComponent } from '@app/modules/results/results.component';

const routes: Routes = [
  { path: 'results', component: ResultsComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
